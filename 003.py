#!/usr/bin/env python3
""" Project Euler
Solution to Problem No. 3"""

def main():
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        answer = int()
        q = n
        if n == 1:
            answer = 1
        # Remove 2 in the number
        while q >= 1:
            if q % 2 != 0:
                break
            q //=2
        # if answer is 2 stop here
        if q == 1:
            print(2)
            continue
        # remove other smaller prime factors
        for i in range(3, int(q ** (.5) )+1, 2):
            while q % i == 0 and q // i != 1:
                q //= i

        print(q)

if __name__ == "__main__":
    main()
