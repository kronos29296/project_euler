#!/bin/python3
""" Project Euler Solution to
    Problem 002 in Hackerrank"""
import sys

def fib(max):
    a, b = 0, 1
    while a < max:
        yield a
        a, b = b, a + b

def main():
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        sum = 0
        for i in fib(n):
            if i % 2 == 0:
                sum += i
        print(sum)

if __name__ == "__main__":
    main()
