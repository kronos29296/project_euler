#!/bin/python3
""" Project Euler
Solution to Problem No. 2"""

class Fib:
    '''iterator that yields numbers in the Fibonacci sequence'''

    def __init__(self, max):
        self.max = max

    def __iter__(self):
        self.a = 0
        self.b = 1
        return self

    def __next__(self):
        fib = self.a
        if fib > self.max:
            raise StopIteration
        self.a, self.b = self.b, self.a + self.b
        return fib

def main():
    sum = 0
    for i in Fib(4000000):
        if i % 2 == 0:
            sum += i
    print(sum)

if __name__ == "__main__":
    main()
