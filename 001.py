#!/bin/python3
""" Project Euler
Solution to Problem No. 1"""

def ap_sum(d, limit):
    """ Calculates the sum of A.P from d to largest below limit.
    (If Limit is not a part of series largest number below it is used.)
    Args - d  : Common difference
        limit : Upper limit for last term of A.P """
    n = limit // d
    l = n * d
    return n * (l + d)//2

def main():
    sum = ap_sum(3,999) + ap_sum(5,999) - ap_sum(15,999)
    print(sum)

if __name__ == "__main__":
    main()
